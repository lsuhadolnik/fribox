window.addEventListener('load', function() {
	//stran nalozena
	
	var prizgiCakanje = function() {
		document.querySelector(".loading").style.display = "block";
	}
	
	var ugasniCakanje = function() {
		document.querySelector(".loading").style.display = "none";
	}
	
	document.querySelector("#nalozi").addEventListener("click", prizgiCakanje);
	
	//Pridobi seznam datotek
	var pridobiSeznamDatotek = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		
		xhttp.open("GET","/datoteke");
		xhttp.send();
		
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				var datoteke = JSON.parse(xhttp.responseText);
				
				var datotekeHTML = document.querySelector("#datoteke");
				
				for (var i=0; i<datoteke.length; i++) {
					var datoteka = datoteke[i];
					
					var round2 = function(dbl){
						return Math.floor(dbl*100)/100;
					}
					
					var formatirajVelikost = function(vel){
						if(vel < 1024) return vel+" B";
						if(vel / 1024 > 0) return round2((vel/1024))+" KiB";
						if(vel / (Math.pow(1024,2))) return round2((vel / (1024*1024)))+" MiB";
						return round2((vel/(Math.pow(1024,3))))+" GiB";
					}
					
					var velikost = formatirajVelikost(datoteka.velikost);
					
					
					datotekeHTML.innerHTML += " \
						<div class='datoteka senca rob'> \
							<div class='naziv_datoteke'> " + datoteka.datoteka + "  (" + velikost + ") </div> \
							<div class='akcije'> \
							<span><a href='/poglej/" + datoteka.datoteka + "' target='_self'>Poglej</a></span> \
							| <span><a href='/prenesi/" + datoteka.datoteka + "' target='_self'>Prenesi</a></span> \
							| <span akcija='brisi' datoteka='"+ datoteka.datoteka +"'>Izbriši</span> </div> \
					    </div>";	
				}
				
				
				if (datoteke.length > 0) {
					var ele = document.querySelectorAll("span[akcija=brisi]");
					for(var i = 0; i < ele.length; i++){
						ele[i].addEventListener("click", brisi);
					}
					
				}
				ugasniCakanje();
			}
		};
	}
	

	
	var brisi = function(event) {
		
		console.log("WUT??");
		
		prizgiCakanje();
		
		
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				if (xhttp.responseText == "Datoteka izbrisana") {
					window.location = "/";
				} else {
					alert("Datoteke ni bilo možno izbrisati!");
				}
				
			}
			ugasniCakanje();
		};
		xhttp.open("GET", "/brisi/"+this.getAttribute("datoteka"), true);
		xhttp.send();
		
	
	}
	
	pridobiSeznamDatotek(this);

});